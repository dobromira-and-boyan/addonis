package com.alpha33.addonis.repositories.contracts;

import com.alpha33.addonis.models.Tag;

import java.util.List;

public interface TagRepository {

    List<Tag> getByName(String name);

    Tag getByNameNew(String tagName);

    void create(Tag tag);

    List<Tag> getAll();
}
