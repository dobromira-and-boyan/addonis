package com.alpha33.addonis.repositories.contracts;

import com.alpha33.addonis.models.Ide;

import java.util.List;

public interface IdeRepository {

    List<Ide> getAll();

    void create(Ide ide);

    Ide getByName(String name);

    Ide getById(int id);
}
