package com.alpha33.addonis.repositories;

import com.alpha33.addonis.models.AddonRating;
import com.alpha33.addonis.repositories.contracts.AddonRatingRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import java.math.BigInteger;
import java.util.List;

@Repository
public class AddonRatingRepositoryImpl implements AddonRatingRepository {

    private final SessionFactory sessionFactory;

    public AddonRatingRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public void create(AddonRating addonRating) {
        try (Session session = sessionFactory.openSession()) {
            session.save(addonRating);
        }
    }

    @Override
    public List<AddonRating> getAll() {
        try (Session session = sessionFactory.openSession()) {
            Query<AddonRating> query = session.createQuery("from AddonRating", AddonRating.class);
            return query.list();
        }
    }

    @Override
    public double calculateRating(int id) {
        double result;
        try (Session session = sessionFactory.openSession()) {
            result = (Double) session.createQuery("SELECT AVG(ratingValue) from AddonRating where addon.id = :id")
                    .setParameter("id", id)
                    .getSingleResult();
        }
        return result;
    }

    @Override
    public long calculateRaters(int id) {
        long number;
        try (Session session = sessionFactory.openSession()) {
            number = (long) session.createQuery("select count (user) from AddonRating where addon.id = :id").
                    setParameter("id", id).getSingleResult();
        }
        return number;
    }

    @Override
    public long[] ratingsByUsers(int id) {

        long[] stars = new long[5];
        long oneStar;
        long twoStars;
        long threeStars;
        long fourStars;
        long fiveStars;
        String sql = "select count(user_id) from users_ratedAddons where addon_id= :id and value= :star";


        try (Session session = sessionFactory.openSession()) {


            BigInteger oneStarBig = (BigInteger) session.createNativeQuery(sql).
                    setParameter("id", id).setParameter("star", 1).uniqueResult();
            oneStar = oneStarBig.longValue();

            BigInteger twoStarBig = (BigInteger) session.createNativeQuery(sql).
                    setParameter("id", id).setParameter("star", 2).uniqueResult();
            twoStars = twoStarBig.longValue();

            BigInteger threeStarBig = (BigInteger) session.createNativeQuery(sql).
                    setParameter("id", id).setParameter("star", 3).uniqueResult();
            threeStars = threeStarBig.longValue();

            BigInteger fourStarBig = (BigInteger) session.createNativeQuery(sql).
                    setParameter("id", id).setParameter("star", 4).uniqueResult();
            fourStars = fourStarBig.longValue();

            BigInteger fiveStarBig = (BigInteger) session.createNativeQuery(sql).
                    setParameter("id", id).setParameter("star", 5).uniqueResult();
            fiveStars = fiveStarBig.longValue();

        }

        stars[0] = oneStar;
        stars[1] = twoStars;
        stars[2] = threeStars;
        stars[3] = fourStars;
        stars[4] = fiveStars;

        return stars;
    }

}

