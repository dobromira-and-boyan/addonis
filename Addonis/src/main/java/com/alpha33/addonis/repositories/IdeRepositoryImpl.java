package com.alpha33.addonis.repositories;

import com.alpha33.addonis.exceptions.EntityNotFoundException;
import com.alpha33.addonis.models.Ide;
import com.alpha33.addonis.repositories.contracts.IdeRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class IdeRepositoryImpl implements IdeRepository {

    private final SessionFactory sessionFactory;

    public IdeRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Ide> getAll() {
        try (Session session = sessionFactory.openSession()) {
            Query<Ide> query = session.createQuery("from Ide", Ide.class);
            return query.list();
        }
    }

    @Override
    public void create(Ide ide) {
        try (Session session = sessionFactory.openSession()) {
            session.save(ide);
        }
    }

    @Override
    public Ide getByName(String name) {
        try (Session session = sessionFactory.openSession()) {
            Query<Ide> query = session.createQuery("from Ide where name = :searchedIde", Ide.class);
            query.setParameter("searchedIde", name);

            List<Ide> result = query.list();
            if (result.size() == 0) {
                throw new EntityNotFoundException("Ide", "name", name);
            }
            return result.get(0);
        }
    }

    @Override
    public Ide getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            Query<Ide> query = session.createQuery("from Ide where id = :searchedId", Ide.class);
            query.setParameter("searchedId", id);

            List<Ide> result = query.list();
            if (result.size() == 0) {
                throw new EntityNotFoundException("Ide", "id", String.valueOf(id));
            }
            return result.get(0);
        }

    }
}
