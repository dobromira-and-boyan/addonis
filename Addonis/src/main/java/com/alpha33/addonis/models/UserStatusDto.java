package com.alpha33.addonis.models;

public class UserStatusDto {


    private int status;

    public UserStatusDto() {
    }

    public UserStatusDto(int status) {
        this.status = status;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}
