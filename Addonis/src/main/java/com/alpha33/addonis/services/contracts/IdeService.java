package com.alpha33.addonis.services.contracts;

import com.alpha33.addonis.models.Ide;

import java.util.List;

public interface IdeService {

    List<Ide> getAll();

    void create(Ide ide);

}
