package com.alpha33.addonis.services;

import com.alpha33.addonis.exceptions.GitHubException;
import com.alpha33.addonis.models.GitHubInfo;
import com.alpha33.addonis.repositories.contracts.AddonRepository;
import com.alpha33.addonis.services.contracts.GitHubService;
import org.kohsuke.github.*;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.Date;
import java.util.List;


@Service
public class GitHubServiceImpl implements GitHubService {

    private final AddonRepository addonRepository;

    public GitHubServiceImpl(AddonRepository addonRepository) {
        this.addonRepository = addonRepository;
    }

    @Override
    public GitHubInfo getGitDetails(String name) {


        final String token = "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";
        GitHub gitHub;

        try {
//            gitHub = GitHub.connectUsingOAuth(token);
            gitHub = new GitHubBuilder().withOAuthToken(token).build();
        } catch (IOException e) {
            e.printStackTrace();
            throw new GitHubException("Please verify your GitHub token");
        }


        String gitHubLink = addonRepository.getByName(name).getOriginLocation();

        String[] linkArr = gitHubLink.replaceAll("https://github.com/", "").split("/");
        String gitUserRepo = linkArr[0] + "/" + linkArr[1];

        try {

            GHRepository gitHubRepository = gitHub.getRepository(gitUserRepo);
            int pullRequests = gitHubRepository.getPullRequests(GHIssueState.OPEN).size();
            int openIssues = gitHubRepository.getOpenIssueCount();
            List<GHCommit> commits = gitHubRepository.listCommits().toList();
            Date lastCommitDate = commits.get(0).getCommitDate();

            GitHubInfo gitHubInfo = new GitHubInfo();
            gitHubInfo.setLastCommitDate(lastCommitDate);
            gitHubInfo.setOpenIssues(openIssues);
            gitHubInfo.setPullRequests(pullRequests);

            gitHubInfo.setLink(gitHubLink);

//            System.out.println(pullRequests);
            return gitHubInfo;
        } catch (IOException e) {
            e.printStackTrace();
            throw new GitHubException("Can not get data from Github!");
        }


    }
}
