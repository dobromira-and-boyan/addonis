package com.alpha33.addonis.enums;

public enum Status {

    PENDING("PENDING"),
    APPROVED("APPROVED"),
    DECLINED("DECLINED");

    private final String displayValue;

    Status(String displayValue) {
        this.displayValue = displayValue;
    }

    public String getDisplayValue() {
        return displayValue;
    }

    @Override
    public String toString() {
        switch (this) {
            case PENDING:
                return "PENDING";
            case APPROVED:
                return "APPROVED";
            case DECLINED:
                return "DECLINED";
            default:
                return "";
        }
    }
}
