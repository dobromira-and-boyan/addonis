package com.alpha33.addonis.controllers.mvc;


import com.alpha33.addonis.controllers.AuthenticationHelper;
import com.alpha33.addonis.enums.Status;
import com.alpha33.addonis.exceptions.AuthenticationFailureException;
import com.alpha33.addonis.exceptions.EntityNotFoundException;
import com.alpha33.addonis.exceptions.UnauthorizedOperationException;
import com.alpha33.addonis.models.Addon;
import com.alpha33.addonis.models.User;
import com.alpha33.addonis.services.UserMapper;
import com.alpha33.addonis.services.contracts.AddonRatingService;
import com.alpha33.addonis.services.contracts.AddonService;
import com.alpha33.addonis.services.contracts.UserService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("/admin")
public class AdminMvcController {

    private final UserService userService;
    private final AddonService addonService;
    private final AddonRatingService addonRatingService;
    private final AuthenticationHelper authenticationHelper;
    private final UserMapper userMapper;

    public AdminMvcController(UserService userService,
                              AddonService addonService,
                              AddonRatingService addonRatingService,
                              AuthenticationHelper authenticationHelper,
                              UserMapper userMapper) {
        this.userService = userService;
        this.addonService = addonService;
        this.addonRatingService = addonRatingService;
        this.authenticationHelper = authenticationHelper;
        this.userMapper = userMapper;
    }


    @GetMapping("/dashboard")
    public String getAllAddons(Model model,
                               @RequestParam(value = "ide", required = false) String ide,
                               @RequestParam(value = "search", required = false) String name,
                               @RequestParam(value = "sort", required = false) String sort,
                               HttpSession session) {

        User admin = getUser(session);
        if (admin == null || !admin.isAdmin()) {
            return "redirect:/auth/login";
        }

        String userImage = getImage(admin);
        List<Addon> approvedAddons = new ArrayList<>();
        List<Addon> pendingAddons = new ArrayList<>();
        List<Addon> declinedAddons = new ArrayList<>();

        try {
            List<Addon> addons;
            if (ide == null && name == null && sort == null) {
//            addons = addonService.getAll();
                addons = addonService.getAll();

            } else {
                addons = addonService.filter(Optional.ofNullable(ide), Optional.ofNullable(name), Optional.ofNullable(sort));
            }
            for (Addon addon : addons) {
                addon.setRoundedRate((int) Math.round(addon.getRating()));
                addon.setImageURI(getAddonImage(addon));
                addon.setRaters((int) addonRatingService.calculateRaters(addon.getId()));
                addon.getUser().setImageURI(getImage(addon.getUser()));

                if (addon.getStatus() == Status.APPROVED) {
                    approvedAddons.add(addon);
                } else if (addon.getStatus() == Status.PENDING) {
                    pendingAddons.add(addon);
                } else if (addon.getStatus() == Status.DECLINED) {
                    declinedAddons.add(addon);
                }
            }

            model.addAttribute("addons", addons);
            model.addAttribute("user", admin);
            model.addAttribute("image_admin", userImage);
            model.addAttribute("approved", approvedAddons);
            model.addAttribute("pending", pendingAddons);
            model.addAttribute("declined", declinedAddons);


            return "pages/dashboard/admin-course-overview";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "pages/404-error";
        }
    }

    @GetMapping("/addons/{id}/change-status")
    public String addonChangeStatus(@PathVariable int id, Model model, HttpSession session) {

        User admin = getUser(session);
        if (admin == null || !admin.isAdmin()) {
            return "redirect:/auth/login";
        }

        try {
            Addon addon = addonService.getById(id);
            if (addon.getStatus().toString().equalsIgnoreCase("APPROVED")) {
                addon.setStatus(Status.DECLINED);
            } else {
                addon.setStatus(Status.APPROVED);
            }
            userService.updateAddonStatus(addon, admin);
            return "redirect:/admin/dashboard";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "pages/404-error";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "pages/401-unauthorized-access";
        }
    }

    @GetMapping("/addons/{id}/approve")
    public String addonApprove(@PathVariable int id, Model model, HttpSession session) {

        User admin = getUser(session);
        if (admin == null || !admin.isAdmin()) {
            return "redirect:/auth/login";
        }

        try {
            Addon addon = addonService.getById(id);
            addon.setStatus(Status.APPROVED);
            userService.updateAddonStatus(addon, admin);
            return "redirect:/admin/dashboard";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "pages/404-error";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "pages/401-unauthorized-access";
        }
    }

    @GetMapping("addons/{id}/decline")
    public String addonDecline(@PathVariable int id, Model model, HttpSession session) {

        User admin = getUser(session);
        if (admin == null || !admin.isAdmin()) {
            return "redirect:/auth/login";
        }

        try {
            Addon addon = addonService.getById(id);
            addon.setStatus(Status.DECLINED);
            userService.updateAddonStatus(addon, admin);
            return "redirect:/admin/dashboard";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "pages/404-error";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "pages/401-unauthorized-access";
        }

    }

    @GetMapping("addons/{id}/featured")
    public String addonFeatured(@PathVariable int id, Model model, HttpSession session) {

        User admin = getUser(session);
        if (admin == null || !admin.isAdmin()) {
            return "redirect:/auth/login";
        }

        try {
            Addon addon = addonService.getById(id);
            if (addon.isFeatured()) {
                addon.setFeatured(0);
            } else if (!addon.isFeatured()) {
                addon.setFeatured(1);
            }

            userService.updateAddonFeatured(addon, admin);
            return "redirect:/admin/dashboard";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "pages/404-error";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "pages/401-unauthorized-access";
        }

    }


    private User getUser(HttpSession session) {
        User user;

        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return null;
        }
        return user;
    }


    private String getImage(User user) {
        String image;
        if (user.getImage() == null) {
            image = null;
        } else {
            image = "data:image/png;base64," + Base64.getEncoder().encodeToString(user.getImage());
        }
        return image;
    }


    private String getAddonImage(Addon a) {
        String image;
        if (a.getImage() == null) {
            image = null;
        } else {
            image = "data:image/png;base64," + Base64.getEncoder().encodeToString(a.getImage());
        }
        return image;
    }

}
