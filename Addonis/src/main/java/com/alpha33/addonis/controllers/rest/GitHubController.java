package com.alpha33.addonis.controllers.rest;


import com.alpha33.addonis.exceptions.GitHubException;
import com.alpha33.addonis.models.GitHubInfo;
import com.alpha33.addonis.services.contracts.GitHubService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import java.io.IOException;

@RestController
@RequestMapping("/addonis/git")
public class GitHubController {


    private final GitHubService gitHubService;


    @Autowired
    public GitHubController(GitHubService gitHubService) {
        this.gitHubService = gitHubService;
    }

    @GetMapping("/{name}")
    public GitHubInfo getInfo(@PathVariable String name) {

        try {
            return gitHubService.getGitDetails(name);
        } catch (GitHubException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (IOException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }
}
