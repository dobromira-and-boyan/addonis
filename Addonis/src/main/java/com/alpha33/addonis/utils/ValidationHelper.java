package com.alpha33.addonis.utils;

import com.alpha33.addonis.exceptions.UnauthorizedOperationException;
import com.alpha33.addonis.models.Addon;
import com.alpha33.addonis.models.User;
import org.springframework.stereotype.Component;

@Component
public class ValidationHelper {

    public static final String USERNAME_NOT_SAME_ERROR = "User with username %s can update only its own data";
    public static final String USER_RIGHTS_ERROR = "You are not allowed for this action. Please check your rights.";

    public void validateUserAndCreator(Addon addon, User user) {
        if ((addon.getUser().getId() != user.getId()) && (!user.isAdmin())) {
            throw new UnauthorizedOperationException((String
                    .format(USERNAME_NOT_SAME_ERROR, user.getUsername())));
        }
    }

    public void validateUserRights(User user) {
        if (user.isBlocked()) {
            throw new UnauthorizedOperationException(USER_RIGHTS_ERROR);
        }
    }
}
