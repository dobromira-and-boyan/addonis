package com.alpha33.addonis.services;

import com.alpha33.addonis.TestHelpers;
import com.alpha33.addonis.models.Tag;
import com.alpha33.addonis.repositories.contracts.TagRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

@ExtendWith(MockitoExtension.class)
public class TagServiceImplTests {

    @Mock
    TagRepository mockTagRepository;

    @InjectMocks
    TagServiceImpl service;

    @Test
    void getAll_should_callRepository() {
        //Arrange
        Mockito.when(mockTagRepository.getAll())
                .thenReturn(new ArrayList<>());

        //Act
        service.getAll();

        //Assert
        Mockito.verify(mockTagRepository, Mockito.times(1)).getAll();
    }

    @Test
    void create_should_callRepository_when_tagWithNameDoesNotExists() {
        // Arrange
        Tag mockTag = TestHelpers.createMockTag();

        // Act
        service.create(mockTag);

        // Arrange
        Mockito.verify(mockTagRepository, Mockito.times(1))
                .create(mockTag);
    }

    @Test
    void create_should_notCallRepository_when_tagWithNameExists() {
        // Arrange
        Tag mockTag = TestHelpers.createMockTag();
        List<Tag> mockTagList = new ArrayList<>();
        mockTagList.add(mockTag);

        Mockito.when(mockTagRepository.getByName(Mockito.anyString()))
                .thenReturn(mockTagList);

        // Act
        service.create(mockTag);

        // Arrange
        Mockito.verify(mockTagRepository, Mockito.times(0))
                .create(mockTag);
    }
}
