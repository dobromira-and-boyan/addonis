# Addonis

Addonis is an Addons Registry web application. Here you can find an extention/addon for your IDE. The application is similar to JetBrains Registry, Visual Studio Marketplace and Eclipse Marketplace.

A register user can:
- Publish their own addons
- Browse addons for their preferred IDE
- Download addons
- Rate existing addons

An admin can:
- Approve/decline addons
- Feature some addons
- Block unblock/user
- Change addon status



![Alt text](Addonis/src/main/resources/templates/assets/Screen Shot 2021-12-14 at 1.20.36 PM.png?raw=true "Index page")

_Instructions how to setup and run the project_:

**Option 1**

Click on Addonis it project link in [aws](http://ec2-3-65-226-30.eu-central-1.compute.amazonaws.com:8080). Register as user and login or login as admin: username:admin, password:Super123!

**Option 2**

You will need JDK 1.8 and MariaDb Database. Gitlab link

Clone the project from Gitlab and build it as a Gradle project. In the db directory you will find create_table_script_update_28.11.sql and run scripts. Create a schema addonisdb and this wiil be the database. Run scripts in your MySql client. The program runs on port 8080. So run it and open http://localhost:8080/ in your browser. You can register from the browser as user or insert an admin in the db.

**Swagger**
http://localhost:8080/swagger-ui.html

